# Focus-docs
Текст диплома, связанный с разработкой видеокодека focus: https://www.npmjs.com/package/@chekushka/focus

# Рендер pdf
```shell
latexmk -output-format=pdf -output-directory={target_directory} src/wip-doc.tex
```